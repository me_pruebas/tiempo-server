'use strict'

const weather = require("../providers/Weather_model");

describe("Weather cache", () => {

    let location ;

    before( "init location weather object", () => {

        location = {
            temperature : 10,
            name: "Chile",
            timezone: "America/Santiago",
            offset: -4,
            code: "CL",
        };

    });

    describe("Seve location by code", () => {

        it("will return true on success", () => {

            let result = weather.saveLocation(location);
            assert.equal(result, true);

        });

        it("will throw error with empty object", () => {

            assert.throw( () => weather.saveLocation({}) );

        });

    });

    describe("get location", () => {

        let locationsLength;
        let location;

        before("Init location result length", ( done ) => {

            location = {
                temperature : 10,
                name: "Chile",
                timezone: "America/Santiago",
                offset: -4,
                code: "CL",
            };

            weather.saveLocation(location);
            locationsLength = weather.getLocationsLength();
            done();

        });

        describe("get location length", () => {

            it("will return location length be a number", () => {

                assert.typeOf(locationsLength, "number");

            });

            it("will return location length", () => {

                assert.equal(locationsLength, 1);

            });

        });

        describe("get all locations",() => {

            it("will return all locations", () => {

                let currArrLocations  = [];
                currArrLocations.push(location);

                let arrLocations = weather.getLocations();

                assert.deepEqual(arrLocations, currArrLocations);

            });

        });

        describe("get location by code",() => {

            let codeNotInCache;

            before(() => {
                codeNotInCache = "UK";
            });

            it("will return weather", () => {

                let resultLocation = weather.getLocation(location.code);

                assert.deepEqual(resultLocation,location);

            });

            it("will return NULL", () => {

                let resultLocation = weather.getLocation(codeNotInCache);

                assert.equal(resultLocation, null);

            });

            it("will throw error", () => {

                assert.throw( () => weather.getLocation() );

            });

        });

    });
});
