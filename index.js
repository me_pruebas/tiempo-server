const WebSocket = require('ws');
const moment = require('moment');
const config = require('./config/socket');
const locationsConfig = require('./config/locations');
const weather = require('./providers/darksky');
const LocationsModel = require('./providers/Locations_model');
const WeatherModel = require('./providers/Weather_model');

const wss = new WebSocket.Server({ port: config.SERVER.PORT });

function noop() {}

function heartbeat() { this.isAlive = true; }

wss.on('connection', (ws, req) => {

  ws.isAlive = true;
  ws.on('pong', heartbeat);
  console.info("init client connection.");
  ws.send(JSON.stringify(WeatherModel.getLocations()));

  ws.on('close', () => { console.info('client disconnected'); });

});



let weatherTimeout = null;
let interval = null;

LocationsModel.save(locationsConfig.LOCATIONS);

async function updateWeather() {

  clearTimeout(weatherTimeout);
  let currHour = moment().format('HH:mm:ss');

  if (wss.clients.size > 0 || WeatherModel.getLocationsLength() == 0 )
  {
    console.info(`${currHour} update weather.`);
    try
    {

      let locations = await LocationsModel.get();
      await weather.get(locations);
      weatherTimeout = setTimeout(updateWeather, config.INERVAL_TO_REFRESH);

    }
    catch(e)
    {
      console.error(e.message);
    }

  }
  else if (wss.clients.size == 0)
  {
    console.info(`${currHour} no clients`);
    weatherTimeout = setTimeout(updateWeather, config.INERVAL_TO_REFRESH);
  }


}

function informWeather() {

  wss.clients.forEach(client => {

    if (client.isAlive === false)
    {
      console.info("close client connection.");
      return client.terminate();
    }

    if (client.readyState === WebSocket.OPEN)
    {
        client.send(JSON.stringify(WeatherModel.getLocations()));
    }

    client.isAlive = false;
    client.ping(noop);

  });

}

updateWeather();
interval = setInterval(informWeather, config.INERVAL_TO_REFRESH);
