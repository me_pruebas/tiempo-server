# Servidor Socket

Permite levantar un servidor socket que entrega la temperatura y hora de distintas ubicaciones.


### Instalación

Instalar las dependencias de node con:

```
npm intall

```

### Configuraciones básicas

* Agregar las configuraciones de **redis** en el archivo *config/redis.js*
* Configurar el puerto, en el que se levantará el servidor, en el archivo *config/socket.js*
* Agregar el **API KEY** de Dark Sky en el archivo *config/darksky.js*

### Correr Servidor

Para lavantar el servidor debe ejecutar el siguiente comando:

```
node index.js

```
