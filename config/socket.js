/*
* Configuraciones del servidor.
* PORT = Puerto en el que levantará el socket
*/
exports.SERVER = {
    PORT: process.env.PORT || 8080
};

/*
* Intervalo de tiempo en milisegundos en que
* se refrescaran los datos en el cliente
*/
exports.INERVAL_TO_REFRESH = 10000;