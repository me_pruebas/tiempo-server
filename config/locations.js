/*
* Lista de ubicaciones que serán consultada en el API
*/
exports.LOCATIONS = [
    {
        name: "Santiago",
        latitude: "-33.4569397",
        longitude: "-70.6482697",
        code: "CL"
    },
    {
        name: "Zurich",
        latitude: "47.3666687",
        longitude: "8.5500002",
        code: "CH"
    },
    {
        name: "Auckland",
        latitude: "-36.8485298",
        longitude: "174.7634888",
        code: "NZ"
    },
    {
        name: "Zydney",
        latitude: "-33.8678513",
        longitude: "151.2073212",
        code: "AU"
    },
    {
        name: "Londres",
        latitude: "51.5085297",
        longitude: "-0.12574",
        code: "UK"
    },
    {
        name: "Georgia",
        latitude: "33.7490005",
        longitude: "-84.3879776",
        code: "USA"
    },
];

/*
* Número de horas en que está el horario local con respecto a UTC-0
*/
exports.CURRENT_OFFSET_TIMEZONE = -4;