exports.ENDPOINT = 'https://api.darksky.net/forecast/';

/*
* clave secreta del API Dark Sky
*/
exports.KEY = '<key>';

/*
* Determina las unidades de medidas que utilizara el API
* si = Sistema Internacional
*/
exports.UNITS = 'si';

/*
* Determina el idioma que utilizará el API en la respuesta
*/
exports.LANGUAGE = 'es';

/*
* 	Indica la cantidad de reintentos al API cuando está falla.
* 	Para detallar el número de reintentos debe ingresar uno de los
* siguientes valores:
*
* - reintentos infinitos: < 0
* - sin reintentos: 0
* - con reintentos: > 0
*/
exports.NUMBER_OF_REQUEST_RETRIES = -1;