'use strict'

let weather = [];

exports.saveLocation = (location) => {

	if(!location.hasOwnProperty("code"))
		throw new Error("location code must be set");

	weather[location.code] = location;

	return true;

};

exports.getLocationsLength = () => {

	return Object.keys(weather).length;

}

exports.getLocations = () => {

	return Object.keys(weather).map( code => weather[code] );

};

exports.getLocation = (locationCode = null) => {

	if(!locationCode)
		throw new Error("locationCode must be set ", locationCode);

	let foundCode = Object.keys(weather).find( code => code == locationCode );

	return (foundCode) ? weather[foundCode]: null;

};