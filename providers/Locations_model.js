const redisClient = require("../utils/redis");

exports.save = (locations) => {

	redisClient.client.set("locations", JSON.stringify(locations) );

};

exports.get = async () => {

	let locations = [];
	try
	{
		let _locations = await redisClient.client.getAsync('locations');
		locations = JSON.parse(_locations);
	}
	catch(e)
	{
		console.error(e.message);
	}

	return locations;

};