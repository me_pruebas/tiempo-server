const moment = require('moment');
const redisClient = require("../utils/redis")

exports.saveAPIError = (e) => {
	let now = moment().unix();
	redisClient.client.hmset('api.errors', now, JSON.stringify( e ) );
};