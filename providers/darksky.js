const config = require('../config/darksky');
const locationsConfig = require('../config/locations');
const fetch = require('node-fetch');
const CustomError = require('../utils/CustomError.js');
const ErrorModel = require('./Error_model');
const WeatherModel = require('./Weather_model');

module.exports.get = async (locations) => {

	if(!locations)
		throw new Error('locations must be an array');

	if(!locations.length)
		throw new Error('locations must be longer than 0');

	let locationsPromises = locations.map( async (location) => {

		let retry = true;
		let countRetries = 0;

		let url = `${config.ENDPOINT}${config.KEY}/${location.latitude},${location.longitude}?units=${config.UNITS}&lang=${config.LANGUAGE}`;

		while(true)
		{
			try
			{
				let probabilityToFail = Math.random();
				if( probabilityToFail < 0.1 )
				{
					throw new CustomError('How unfortunate! THE API Request Faied');
				}

				let response = await fetch(url);

				if( response.ok )
				{

					weather = await response.json();

					let response_weather = {
						temperature : Math.round(weather.currently.temperature),
						name: location.name,
						timezone: weather.timezone,
						offset: weather.offset - locationsConfig.CURRENT_OFFSET_TIMEZONE,
						code: location.code,
					};

					WeatherModel.saveLocation(response_weather);
					return response_weather;

				}
				else
				{
					throw new Error(response.statusText);
				}

			}
			catch(e)
			{
				if(e.name == "CustomError")
				{
					ErrorModel.saveAPIError({
						error: {
							message: e.message,
							stack: e.stack,
						},
						request_url: url
					});
				}

				console.error( e.message );

				if((config.NUMBER_OF_REQUEST_RETRIES < 0 ) || ( config.NUMBER_OF_REQUEST_RETRIES > 0 && retry < config.NUMBER_OF_REQUEST_RETRIES) )
				{
					countRetries++;
				}
				else
				{
					return WeatherModel.getLocation(location.code);
				}

			}
		}


	});

	return await Promise.all(locationsPromises);

};