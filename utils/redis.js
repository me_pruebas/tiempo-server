const redis = require("redis");
const util = require('util');
const config = require("../config/redis");

const client = redis.createClient(config.PORT, config.HOST);

client.getAsync = util.promisify(client.get).bind(client);

client.on('error', (error) => {
	console.log("Error al iniciar redis = ", error);
	process.exit();
})

exports.client = client;
